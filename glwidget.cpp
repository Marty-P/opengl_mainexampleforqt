#include "glwidget.h"

GLWidget::GLWidget(QWidget *parent) :
    QGLWidget(parent)
{
}

void GLWidget::initializeGL()
{
    glClearColor(0.5,0.5,0.5,1);
}

void GLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT);

    glBegin(GL_POLYGON);
    glColor3d(1,0,0);
    glVertex2d(-0.5,-0.5);
    glColor3d(0,1,0);
    glVertex2d(0,0.5);
    glColor3d(0,0,1);
    glVertex2d(0.5,-0.5);
    glEnd();

}

void GLWidget::resizeGL(int w, int h)
{
    glViewport(0,0,w,h);

}
